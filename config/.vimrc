" Configuration file for vim

set nocompatible
filetype off

"sometimes used temporary:
"set paste

set textwidth=120
set colorcolumn=120

set ignorecase

set tabpagemax=200
set swb=usetab " use :sb to jump to a file per name

" To start vundle use:
" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" https://github.com/gmarik/vundle
"
" Bundles are in .vim/bundle and .vim/autoload? (but not .vim/plugin)
"
" :BundleInstall       - install bundles (note to add them below in config)
"                        To install new bundles, use :BundleInstall and press tab
" :BundleInstall!      - update all bundles
" :BundleList          - list configured bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or
" https://github.com/gmarik/vundle/blob/master/doc/vundle.txt
" NOTE: comments after Bundle command are not allowed..
"
" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" CLang completion (not used anymore)
"Bundle 'clang-complete'

" Use :NERDTree to show the directory layout
"
"    Use the natural vim navigation keys hjkl to navigate the files.
"    Press o to open the file in a new buffer or open/close directory.
"    Press t to open the file in a new tab.
"    Press i to open the file in a new horizontal split.
"    Press s to open the file in a new vertical split.
"    Press p to go to parent directory.
"    Press r to refresh the current directory.
"
" See http://www.catonmat.net/blog/vim-plugins-nerdtree-vim/
Bundle 'The-NERD-tree'

" Finds a project together with The-NERD_tree
Bundle 'NERD_tree-Project'

" autocomplete code snippets
Bundle 'snipMate'

" Use :A to quickly change between header/source
"
" :AS splits and switches
" :AV vertical splits and switches
" :AT new tab and switches
" :AN cycles through matches
" :IH switches to file under cursor
" :IHS splits and switches
" :IHV vertical splits and switches
" :IHT new tab and switches
" :IHN cycles through matches 
Bundle 'a.vim'

" makes % much more powerful
" (jump to anything what opens or closes)
Bundle 'matchit.zip'

" Doxygen support
" use :Dox to document a function
"
" see .vim/bundle/DoxygenToolkit.vim/README
"
" Config for DoxygenToolkit:
"
"let g:DoxygenToolkit_commentType="C++"
"let g:DoxygenToolkit_briefTag_pre=""
"let g:DoxygenToolkit_paramTag_pre="\\param "
"let g:DoxygenToolkit_returnTag="\\return "
Bundle 'DoxygenToolkit.vim'

" \cc .. commit to repository
" \ca .. add to repository
" \cD .. delete from repository
" \cs .. status of repository
" \cd .. show diff
" \cv .. show vimdiff
" \cl .. show log
Bundle 'git://repo.or.cz/vcscommand'


" Needed for bundle:
filetype plugin indent on




"
" END OF BUNDLE CONFIGURATION
"





"show options when pressing tab
set wildmenu
set wildmode=list:longest

"sometimes used temporary:
"set paste
set textwidth=72
set ic

function! Kdb(var)
	let shellcmd='kdb get user/sw/vim/' . a:var
	let output=system(shellcmd)
	return output
endfunction


set backupdir=~/tmp,/var/tmp,/tmp,.
set directory=~/tmp,/var/tmp,/tmp,.

function! KdbList(var)
	let shellcmd='for i in `kdb ls user/sw/vim/' . a:var . '`; do echo -n ","; kdb get -n "$i"; done'
	let output=system(shellcmd)
	return output
endfunction

set dictionary=/usr/share/dict/words
set complete=i
"set tags=/home/markus/.ctags,.ctags
let &tags=KdbList('tags')
"let shellcmd='for i in `kdb ls user/sw/vim/path`; do echo -n ","; kdb get -n "$i"; done'
"let output=system(shellcmd)
"let &path='.' . output
let &path='.' . KdbList('path')

"do not modify runtimepath, instead use vim-addons install..
"set runtimepath=~/.vim,/usr/share/vim/addons,/usr/share/vim/vimfiles,/usr/share/vim/vim72,/usr/share/vim/vimfiles/after,~/.vim/after


" Normally we use vim-extensions. If you want true vi-compatibility
" remove change the following statements
syntax enable

"grey tab markers
set listchars=tab:>-,trail:.
set list
highlight SpecialKey ctermbg=Black ctermfg=DarkGrey

"reox tipps
set nu

"map <F7> :'<,'>!xclip<CR><CR>
"vmap <F7> "*y
"map <S-F7> :r!xclip -o<CR>


au! Syntax {cpp,c,idl}
au Syntax {cpp,c,idl} runtime syntax/doxygen.vim

set spell		" Use spell correction
set nocompatible	" Use Vim defaults instead of 100% vi compatibility
set backspace=indent,eol,start	" more powerful backspacing
set cindent
"set textwidth=0		" Don't wrap words by default
set nobackup		" Don't keep a backup file
set viminfo='20,\"1000	" read/write a .viminfo file, don't store more than
			" 1000 lines of registers
set history=1000	" keep 1000 lines of command line history
set ruler		" show the cursor position all the time

hi SpellBad ctermfg=White ctermbg=Black cterm=underline
hi SpellCap ctermfg=White ctermbg=Black cterm=underline

" Set xterm title
"set title

" Copy and paste for X
" You need to install xsel for it
" (even without X support. If you have X support use the * register)
command -range Xc :silent :<line1>,<line2>w !xsel -i
command -range Xp :silent :r !xsel -o

" Define shortcuts
" (is needed because user commands must start uppercase)
ca xc Xc
ca xp Xp


" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc


" Debian uses compressed helpfiles. We must inform vim that the main
" helpfiles is compressed. Other helpfiles are stated in the tags-file.
" set helpfile=$VIMRUNTIME/doc/help.txt

" For reportbug
augroup filetype
  au BufRead reportbug.*		set ft=mail
  au BufRead reportbug-*		set ft=mail
augroup END

" The following are commented out as they cause vim to behave a lot
" different from regular vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hls			" Make highlights coloured
set magic		" Interprets '*.' in patterns

" Functions
let spellst = ["de", "en_gb"]
let langcnt = 0
function Sel_lang()
	let g:langcnt = (g:langcnt+1) % len(g:spellst)
	let lang = g:spellst[g:langcnt]
	" echo "language " . lang . " selected"
	exe "set spelllang=" . lang
endfunction

" Choose german as default
"call Sel_lang()
"call Sel_lang()

" http://vim.wikia.com/wiki/Search_across_multiple_lines
" Search for the ... arguments separated with whitespace (if no '!'),
" or with non-word characters (if '!' added to command).
function! SearchMultiLine(bang, ...)
  if a:0 > 0
    let sep = (a:bang) ? '\_W\+' : '\_s\+'
    let @/ = join(a:000, sep)
  endif
endfunction
command! -bang -nargs=* -complete=tag S call SearchMultiLine(<bang>0, <f-args>)|normal! /<C-R>/<CR>
" use :S <words..> or :S! <words..>

"From gentoo:
" When editing a file, always jump to the last cursor position
 autocmd BufReadPost *
       \ if ! exists("g:leave_my_cursor_position_alone") |
       \    if line("'\"") > 0 && line ("'\"") <= line("$") |
       \        exe "normal g'\"" |
       \    endif |
       \ endif

" Mappings
map <C-[C> <w> " next word
map <C-[D> <b> " prev word
nmap <Esc>l :call Sel_lang()<CR>

nmap <C-left> :tabprevious<cr> 
map <C-left> :tabprevious<cr> 
imap <C-left> <c-o>:tabprevious<cr>

nmap <C-right> :tabnext<cr> 
map <C-right> :tabnext<cr> 
imap <C-right> <c-o>:tabnext<cr>

nmap <C-t> :tabnew<cr> 
imap <C-t> <ESC>:tabnew<cr> 

" \ has an awkward position on german keyboards, so lets use something else!
let mapleader=","
map <C-l> <C-]>	" Follow _L_ink on Ctrl+l

" OmniCppComplete
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
" automatically open and close the popup menu / preview window
"au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview




source ~/.vimrc-local
