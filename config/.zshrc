#.zshrc
# code to execute at shell startup


#elektra configuration:
#
# should replace aliases, exports and hashes below
#
# Source functions in /etc/profile.d/
#
#for i in /etc/profile.d/* ; do
#    if [ -r "$i" ]; then
#        #echo sourcing $i
#        source $i
#    fi
#done

#some aliases
alias a="sudo aptitude"		# run aptitude
alias d="diff -pruN"		# differences between files
alias r="recode latin1..utf8"	# recode a file
alias s="festival --tts"	# speak something out
alias p='ps -fu $USER'		# show own processes
alias u='uptime'		# show how long system is up
alias l="ls"			# spell correction of ls
alias s="ls"			# spell correction of ls
alias v="valgrind --leak-check=full --show-reachable=yes"
alias up="sudo ifup"
alias no="yes n"		# why not having no too?
alias ls="ls --color=auto"	# make ls colourful
alias sl="ls"			# spell correction of ls too
alias mv='nocorrect mv'		#
alias cp='nocorrect cp'		#
alias rm='nocorrect rm'		#
alias du="du -h --max-depth=1"	#
alias cd/='cd /'		# forgot a space for cd
alias cd..='cd ..'		# forgot a space for cd
alias mkdir='nocorrect mkdir'	#
alias man='nocorrect man'	#
alias kdb='nocorrect kdb'	#
alias find='noglob find'	#
alias make='nocorrect make'	# do not correct make test to tests if "tests" folder is present
alias down="sudo ifdown"
alias diet-gcc="diet gcc -s -Os -pipe"
alias remod="sudo rmmod ipw2200 && sudo modprobe ipw2200 "
alias list="sudo iwlist eth1 scan" 
alias gping="ping www.google.at"
alias starte="kfmclient exec"
alias xfig="xfig -specialtext -latexfonts -startlatexFont default"
alias lintian="lintian -EI --color=auto --pedantic"
alias reprepro="reprepro -b ~reprepro"

#pipes stderr
alias -g P='>&2 2>&1 |'

# Creates a directory and changes into it at one command
function mkcd() {
	[ -n "$1" ] && mkdir -p "$@" && cd "$1"
}

#some exports
#(more in .zshrc-local or elektra)
export GCC="gcc"
export EDITOR="vim"
export PAGER="less"
export GTK2_RC_FILES=$HOME/.gtkrc-2.0
export LD_LIBRARY_PATH=$HOME/lib:/usr/lib/debug
export PKG_CONFIG_PATH=$HOME/lib/pkgconfig
export LINTIAN_LAB=$HOME/.lintian
export CFLAGS=-Wall
export XMMS_PATH="tcp://audio:9876"
#export BIBINPUTS=$HOME/Info/BibTeX

#some hashes
#(more in .zshrc-local or elektra)
hash -d m=/home/user/MyDocs

HISTFILE=${HOME}/.zsh_history	# name of history file
HISTSIZE=5000			# internal history (larger for duplicated events)
SAVEHIST=4500			# history in files
DIRSTACKSIZE=20			# stack of directories
MAIL=/var/mail/$USERNAME	# where are new mails?
MAILCHECK=30			# check mails every 30 sec
LOGCHECK=30			# check login/logout every 30 sec

unset TMOUT			# don't timeout the session

bindkey -v                      # vi key bindings

bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward
# Use up and down key to search in the history with the current text
# !word<tab> will autocomplete in the history
# (up and down key can be used afterwards too)
# use <ESC> + / to search backwards in the history (? to search forward)
# (n(next) and N(previous) will scroll in the history in this mode)


setopt always_to_end		# move to end of word when completing
setopt append_history		# don't overwrite history file (multiple!)
setopt share_history		# share history between multiple sessions
setopt auto_cd			# change to directory when entering it as command
setopt auto_menu		# use menu completion
setopt auto_pushd		# cd will push old directory on stack
setopt NO_hup			# don't HUP running jobs when shell exits
setopt NO_beep			# don't beep around
setopt NO_hist_beep		# also don't beep at no history
setopt NO_list_beep		#
setopt correct_all		# correct all arguments in a line
setopt extended_glob		# tread ~, ^ as part of patterns
setopt extended_history		# save timestamps of history
setopt glob_complete		# cycle through glob possibilities
setopt hist_expire_dups_first	# duplicate internal events
setopt hist_find_no_dups	# but don't find duplicates
setopt hist_ignore_all_dups	# and ignore them
setopt hist_ignore_dups		# and don't put same commands to history
setopt hist_ignore_space	# first character a space -> don't put to history
setopt hist_reduce_blanks	# reduce blanks from command line in history
setopt hist_save_no_dups	# older commands that duplicate are omitted
setopt hist_verify		# perform history expansion and reload line
setopt interactive_comments	# i always want comments
setopt list_packed		# put list in columns
setopt list_types		# show trailing identifying mark
setopt null_glob		# delete globs without match instead of asking
setopt pushd_ignore_dups	# ignore dups on directory stack
setopt pushd_silent		# be silent after pushd and popd


zstyle ':completion:*:cd:*' ignore-parents parent pwd #cd will never select the parent directory (e.g.: cd ../<TAB>):

#PATH exportieren
PATH="$PATH:/usr/lib/git-core"
PATH="$PATH:/usr/local/bin"
PATH="$PATH:/usr/games"
PATH="$PATH:/usr/bin"
PATH="$PATH:/bin"
PATH="$PATH:$HOME/bin"
PATH="$PATH:."
export PATH

# prompt, right prompt, don't see XX possibilities and spelling prompt
EXITCODE="%(?..%?%1v )"
#old one without exit codes:
#PS1=$'%{\e[0;36m%}%n@%m:%4c%1v%{\e[0m%}> '
PS1=$'%(?..%?%1v )%{\e[0;36m%}%n@%m:%4c%1v%{\e[0m%}> '
RPS1=$'%{\e[0;36m%}%D{%A %T}%{\e[0m%}'
LISTPROMPT=''
SPROMPT='zsh: correct '%R' to '%r' ? ([Y]es/[N]o/[E]dit/[A]bort) '

#activate coredumps
ulimit -c unlimited

#load other modules from ~/.zshfiles
fpath=(~/.zshfiles $fpath)
autoload -U ~/.zshfiles*(:t)

#load compinit
autoload -U compinit		# full features tab completion
compinit -C			# start the tab completion
zmodload -i zsh/complist	# Color completion (For menu selection)

#load local files
. ~/.zshrc-local

#start up screen, if not already in
#if [ -z $STY ]; then
#        screen -D -R
#fi
